﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Novo.Models.Entity
{
    public class Leaves : Base
    {
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Leave Start Date")]
        public string StartDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Leave End Date")]
        public string EndDate { get; set; }
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }
        //[Required]
        [Display(Name = "Employee Email")]
        public string EmployeeEmail { get; set; }
        [Required]
        [Display(Name = "Leave Type")]
        public int LeaveTypeId { get; set; }
        public LeaveType LeaveType { get; set; }
        //[Required]
        [Display(Name = "Purpose of Leave Request")]
        public string Purpose { get; set; }
        //[Required]
        [Display(Name = "Unit Head Approved?")]
        public bool isUnitHeadApproved { get; set; }
        //[Required]
        [Display(Name = "HR Approved?")]
        public bool isHrApproved { get; set; }
        //[Required]
        [Display(Name = "Total Leave")]
        public int TotalLeave { get; set; }
        //[Required]
        [Display(Name = "Leave Utilized")]
        public int LeaveUtilized { get; set; }
        //[Required]
        [Display(Name = "Leave Remaining")]
        public int LeaveRemaining { get; set; }
        //[Required]
        [Display(Name = "Leave Request Status")]
        public string LeaveRequestStatus { get; set; }
        [Required]
        [Display(Name = "Employee Unit")]
        public int UnitsId { get; set; }
        [ForeignKey("UnitsId")]
        public Units Units { get; set; }
        [Display(Name = "Leave HandOver Note")]
        [DataType(DataType.MultilineText)]
        public string HandOver { get; set; }

        [Display(Name = "Unit Head Disapproved?")]
        public bool isUnitHeadDisapprove { get; set; }
        //[Required]
        [Display(Name = "HR Disapproved?")]
        public bool isHrDisapprove { get; set; }

    }
}