﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.Models.Entity
{
    public class State : Base
    {
    
        public string Name { get; set; }
        public string Code { get; set; }
        public Regions Regions { get; set; }
        
    
    }
}
