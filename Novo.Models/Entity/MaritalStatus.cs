﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Novo.Models.Entity
{
    [Table("MaritalStatus")]
    public partial class MaritalStatus : Base
    {
      
        public string Name { get; set; }
        public Guid Guid { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
