﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novo.Models.Entity
{
    public class Departments : Base
    {

        public string Name { get; set; }

     
        [Display(Name = "Dept Head")]
        public ApplicationUser DeptHead { get; set; }

    }
}
