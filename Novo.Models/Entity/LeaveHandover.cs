﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.Entity
{
   public class LeaveHandover : Base
    {
        [Display(Name = "Leave")]
        public int LeaveId { get; set; }

        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }
        public string Employee { get; set; }
        [Required]
        [Display(Name = "Leave Handover Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
    }
}
