﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.Models.Entity
{
    public class Support : Base
    {

        public int EmployeeId { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
        public string TreatedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
    
}
