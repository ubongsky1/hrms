#pragma checksum "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2f1bea9f9e3d68516042bfc7116769a7788c5290"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tasks_Index), @"mvc.1.0.view", @"/Views/Tasks/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tasks/Index.cshtml", typeof(AspNetCore.Views_Tasks_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp;

#line default
#line hidden
#line 2 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp.Moddel;

#line default
#line hidden
#line 3 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using Novo.Models.Entity;

#line default
#line hidden
#line 3 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
using Novo.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2f1bea9f9e3d68516042bfc7116769a7788c5290", @"/Views/Tasks/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c88feaf465e480a3f60b1fca67186b9ca2f0114", @"/Views/_ViewImports.cshtml")]
    public class Views_Tasks_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Novo.Models.Entity.MyTask>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("text-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/Tasks/Create"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-facebook btn-rounded mb-4"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationSummaryTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(105, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(211, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
  
    ViewData["Title"] = "Staff List";
    var user = await UserManager.GetUserAsync(User);

#line default
#line hidden
            BeginContext(313, 2494, true);
            WriteLiteral(@"<section id=""main-content"">
    <section class=""wrapper"">
        <div class=""row mb"">
            <div class=""col-lg-12"">
                <div class=""content-panel border rounded"">

                    <div class=""adv-table"">
                        <div class=""row"">
                            <div class=""col-lg-12"">

                            </div>
                        </div>
                        <br />

                        <div class=""row"">
                            <div class=""col-lg-12"">
                                <div class=""col col-md-6"">
                                    <table>
                                        <tr>
                                            <td>
                                                <i class=""fa fa-gears
                                                    fa-4x""></i>
                                            </td>
                                            <td>
                                                <h4>
  ");
            WriteLiteral(@"                                                  Task
                                                </h4>
                                                <p>&nbsp;&nbsp;&nbsp; Manage Task
                                            </td>
                                        </tr>
                                    </table>


                                </div>
                                <div class=""col col-md-6"">


                                    <ol class=""breadcrumb"">
                                        <li>
                                            <a href=""#"">
                                                <i class=""fa fa-gears
                                                           ""></i> Task

                                            </a>
                                        </li>
                                        <li><a href=""#"">Index</a></li>

                                    </ol>
                                </div>
              ");
            WriteLiteral(@"              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=""row mb"">
            <div class=""modal fade"" id=""myModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""myModalLabel"" aria-hidden=""true"">
                <div class=""modal-dialog"">
                    <div class=""modal-content"">
                        <div class=""modal-header"">
");
            EndContext();
            BeginContext(3027, 167, true);
            WriteLiteral("                        </div>\r\n                        <div class=\"modal-body\">\r\n                            <div class=\"col-md-12\">\r\n                                ");
            EndContext();
            BeginContext(3194, 3079, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "381191a3c853493fa827144a211ebbe0", async() => {
                BeginContext(3220, 38, true);
                WriteLiteral("\r\n                                    ");
                EndContext();
                BeginContext(3258, 66, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("div", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b248c2595c0f41c7956f3ca21f109c2c", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationSummaryTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper);
#line 78 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper.ValidationSummary = global::Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary.ModelOnly;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-validation-summary", __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper.ValidationSummary, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3324, 58, true);
                WriteLiteral("\r\n                                    <br /><br /><br />\r\n");
                EndContext();
                BeginContext(3985, 1113, true);
                WriteLiteral(@"                                    <div class=""form-group"">

                                        <div class=""col-sm-10"">
                                            <label>Staff Fullname</label>
                                            <input type=""text"" name=""StaffFullname""
                                                   value="""" placeholder=""Enter Staff Full Name "" class=""form-control"">

                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class=""form-group"">

                                        <div class=""col-sm-10"">
                                            <label>
                                                Staff Id
                                            </label>
                                            <input type=""text"" name=""StaffId"" value="""" placeholder=""Enter Staff Id"" class=""form-control"" required>
                       ");
                WriteLiteral("                 </div>\r\n                                    </div><br /><br /><br />\r\n\r\n");
                EndContext();
                BeginContext(5696, 176, true);
                WriteLiteral("\r\n\r\n\r\n                                    <div class=\"form-group\" hidden>\r\n                                        <input type=\"datetime\" name=\"CreateDate\" class=\"form-control\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 5872, "\"", 5893, 1);
#line 120 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
WriteAttributeValue("", 5880, DateTime.Now, 5880, 13, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginWriteAttribute("placeholder", " placeholder=\"", 5894, "\"", 5921, 1);
#line 120 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
WriteAttributeValue("", 5908, DateTime.Now, 5908, 13, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(5922, 344, true);
                WriteLiteral(@" />
                                    </div>

                                    <br /><br />

                                    <div class=""form-group"">
                                        <input type=""submit"" value=""Save"" class=""btn btn-facebook"" />
                                    </div>
                                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6273, 341, true);
            WriteLiteral(@"
                            </div>
                        </div>
                        <div class=""modal-footer"">


                        </div>
                    </div>
                </div>
            </div>

        </div>





        <div class=""row mb"">
            <div class=""col-lg-12"">
                ");
            EndContext();
            BeginContext(6614, 150, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d0a3c1e73d9345b4b4b56c241d26959f", async() => {
                BeginContext(6681, 79, true);
                WriteLiteral("\r\n                    <i class=\"fa fa-plus\"></i> Add New Task\r\n                ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6764, 497, true);
            WriteLiteral(@"
                <div class=""content-panel border rounded"">

                    <div class=""adv-table"">
                        <p>
                            <br /><br />
                        </p>
                        <table id=""datatable"" class=""table-bordered table-striped table-responsive"" cellspacing=""0"" width=""97%"">
                            <thead>
                                <tr>
                                    <th>
                                        ");
            EndContext();
            BeginContext(7262, 44, false);
#line 160 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.TaskName));

#line default
#line hidden
            EndContext();
            BeginContext(7306, 127, true);
            WriteLiteral("\r\n                                    </th>\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(7434, 44, false);
#line 163 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.TaskType));

#line default
#line hidden
            EndContext();
            BeginContext(7478, 131, true);
            WriteLiteral("\r\n                                    </th>\r\n\r\n\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(7610, 53, false);
#line 168 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.ExpectedStartDate));

#line default
#line hidden
            EndContext();
            BeginContext(7663, 127, true);
            WriteLiteral("\r\n                                    </th>\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(7791, 51, false);
#line 171 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.ExpectedEndDate));

#line default
#line hidden
            EndContext();
            BeginContext(7842, 127, true);
            WriteLiteral("\r\n                                    </th>\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(7970, 45, false);
#line 174 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.StartDate));

#line default
#line hidden
            EndContext();
            BeginContext(8015, 127, true);
            WriteLiteral("\r\n                                    </th>\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(8143, 43, false);
#line 177 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.EndDate));

#line default
#line hidden
            EndContext();
            BeginContext(8186, 129, true);
            WriteLiteral("\r\n                                    </th>\r\n\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(8316, 42, false);
#line 181 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.Status));

#line default
#line hidden
            EndContext();
            BeginContext(8358, 129, true);
            WriteLiteral("\r\n                                    </th>\r\n\r\n                                    <th>\r\n                                        ");
            EndContext();
            BeginContext(8488, 46, false);
#line 185 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                   Write(Html.DisplayNameFor(model => model.CreateDate));

#line default
#line hidden
            EndContext();
            BeginContext(8534, 208, true);
            WriteLiteral("\r\n                                    </th>\r\n\r\n                                    <th></th>\r\n                                </tr>\r\n                            </thead>\r\n                            <tbody>\r\n");
            EndContext();
#line 192 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                 foreach (var item in Model)
                                {

#line default
#line hidden
            BeginContext(8839, 132, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(8972, 43, false);
#line 196 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.TaskName));

#line default
#line hidden
            EndContext();
            BeginContext(9015, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(9155, 43, false);
#line 199 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.TaskType));

#line default
#line hidden
            EndContext();
            BeginContext(9198, 179, true);
            WriteLiteral("\r\n                                        </td>\r\n                                      \r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(9378, 52, false);
#line 203 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.ExpectedStartDate));

#line default
#line hidden
            EndContext();
            BeginContext(9430, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(9570, 50, false);
#line 206 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.ExpectedEndDate));

#line default
#line hidden
            EndContext();
            BeginContext(9620, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(9760, 44, false);
#line 209 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.StartDate));

#line default
#line hidden
            EndContext();
            BeginContext(9804, 139, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(9944, 42, false);
#line 212 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.EndDate));

#line default
#line hidden
            EndContext();
            BeginContext(9986, 217, true);
            WriteLiteral("\r\n                                        </td>\r\n                                     \r\n                                     \r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(10204, 41, false);
#line 217 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.Status));

#line default
#line hidden
            EndContext();
            BeginContext(10245, 178, true);
            WriteLiteral("\r\n                                        </td>\r\n                                     \r\n                                        <td>\r\n                                            ");
            EndContext();
            BeginContext(10424, 45, false);
#line 221 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                       Write(Html.DisplayFor(modelItem => item.CreateDate));

#line default
#line hidden
            EndContext();
            BeginContext(10469, 714, true);
            WriteLiteral(@"
                                        </td>
                                     
                                        <td>
                                            <div class=""btn-group"">
                                                <button type=""button"" class=""btn btn-purple dropdown-toggle btn-xs"" data-toggle=""dropdown"" aria-expanded=""true"">
                                                    action <span class=""caret""></span>
                                                </button>
                                                <ul class=""dropdown-menu"" role=""menu"">
                                                    <li>
                                                        ");
            EndContext();
            BeginContext(11183, 112, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5d0d58bf70e14244bbee2a4c65262a7e", async() => {
                BeginContext(11240, 51, true);
                WriteLiteral("<i class=\"fa fa-eye\" style=\"color:blue\"></i>Details");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 231 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                                                                  WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(11295, 175, true);
            WriteLiteral("\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        ");
            EndContext();
            BeginContext(11470, 110, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a31f245a7e144d73b4fa7ff8940cf653", async() => {
                BeginContext(11524, 52, true);
                WriteLiteral("<i class=\"fa fa-pencil\" style=\"color:blue\"></i> Edit");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_6.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_6);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 234 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                                                               WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(11580, 260, true);
            WriteLiteral(@"
                                                    </li>

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
");
            EndContext();
#line 241 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\Tasks\Index.cshtml"
                                }

#line default
#line hidden
            BeginContext(11875, 288, true);
            WriteLiteral(@"                        </table>
                        <p>
                            <br /><br />
                        </p>
                    </div>
                </div>
            </div>

            <!-- /col-lg-12 -->
        </div>
    </section>
</section>

");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<ApplicationUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Novo.Models.Entity.MyTask>> Html { get; private set; }
    }
}
#pragma warning restore 1591
