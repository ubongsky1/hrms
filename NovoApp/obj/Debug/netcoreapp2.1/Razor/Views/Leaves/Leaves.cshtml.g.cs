#pragma checksum "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d2d3be92dd1034bb1332f62db98083ade7e63068"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Leaves_Leaves), @"mvc.1.0.view", @"/Views/Leaves/Leaves.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Leaves/Leaves.cshtml", typeof(AspNetCore.Views_Leaves_Leaves))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp;

#line default
#line hidden
#line 2 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp.Moddel;

#line default
#line hidden
#line 3 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using Novo.Models.Entity;

#line default
#line hidden
#line 3 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
using Novo.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d2d3be92dd1034bb1332f62db98083ade7e63068", @"/Views/Leaves/Leaves.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c88feaf465e480a3f60b1fca67186b9ca2f0114", @"/Views/_ViewImports.cshtml")]
    public class Views_Leaves_Leaves : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Novo.Models.Entity.Leaves>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("dropdown-item"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(105, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(211, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
  
    ViewData["Title"] = "Staff List";
    var user = await UserManager.GetUserAsync(User);

#line default
#line hidden
            BeginContext(313, 287, true);
            WriteLiteral(@"

<div class=""row m-auto"">
    <div class=""col-md-10 m-auto grid-margin"">
        <div class=""card"">
            <div class=""card-body"">
                <nav aria-label=""breadcrumb"">
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item"">");
            EndContext();
            BeginContext(600, 52, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "417f87b7dc8f420c96f9d56a1b2c6734", async() => {
                BeginContext(644, 4, true);
                WriteLiteral("Home");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(652, 254, true);
            WriteLiteral("</li>\r\n                        <li class=\"breadcrumb-item active\" aria-current=\"page\">Leave List</li>\r\n                    </ol>\r\n                </nav>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-12 table-responsive-md\">\r\n");
            EndContext();
            BeginContext(974, 710, true);
            WriteLiteral(@"                        <table class=""table table-striped"">
                            <thead>
                                <tr>
                                    <th>Employee</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Leave Purpose</th>
                                    <th>Unit Head</th>
                                    <th>HR</th>
                                    <th>Status</th>
                                    <th class=""text-warning"">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
");
            EndContext();
#line 42 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                 foreach (var item in Model)
                                {

#line default
#line hidden
            BeginContext(1781, 86, true);
            WriteLiteral("                                    <tr>\r\n                                        <td>");
            EndContext();
            BeginContext(1868, 18, false);
#line 45 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                       Write(item.EmployeeEmail);

#line default
#line hidden
            EndContext();
            BeginContext(1886, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(1938, 14, false);
#line 46 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                       Write(item.StartDate);

#line default
#line hidden
            EndContext();
            BeginContext(1952, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2004, 12, false);
#line 47 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                       Write(item.EndDate);

#line default
#line hidden
            EndContext();
            BeginContext(2016, 51, true);
            WriteLiteral("</td>\r\n                                        <td>");
            EndContext();
            BeginContext(2068, 12, false);
#line 48 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                       Write(item.Purpose);

#line default
#line hidden
            EndContext();
            BeginContext(2080, 53, true);
            WriteLiteral("</td>\r\n                                        <td>\r\n");
            EndContext();
#line 50 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                             if (item.isUnitHeadDisapprove == true)
                                            {

#line default
#line hidden
            BeginContext(2265, 68, true);
            WriteLiteral("                                                <p>Disapproved</p>\r\n");
            EndContext();
#line 53 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }
                                            else if (item.isUnitHeadApproved == true && item.isUnitHeadDisapprove == false)
                                            {

#line default
#line hidden
            BeginContext(2552, 65, true);
            WriteLiteral("                                                <p>Approved</p>\r\n");
            EndContext();
#line 57 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
            BeginContext(2761, 74, true);
            WriteLiteral("                                                <p>Awaiting Approval</p>\r\n");
            EndContext();
#line 61 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }

#line default
#line hidden
            BeginContext(2882, 44, true);
            WriteLiteral("                                            ");
            EndContext();
            BeginContext(3088, 95, true);
            WriteLiteral("\r\n                                        </td>\r\n                                        <td>\r\n");
            EndContext();
#line 72 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                             if (item.isUnitHeadDisapprove == true)
                                            {

#line default
#line hidden
            BeginContext(3315, 74, true);
            WriteLiteral("                                            <p>Unit Head Disapproved</p>\r\n");
            EndContext();
#line 75 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }
                                            else if (item.isHrApproved == false)
                                            {

#line default
#line hidden
            BeginContext(3565, 65, true);
            WriteLiteral("                                            <p>Not Approved</p>\r\n");
            EndContext();
#line 79 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }
                                            else
                                            {

#line default
#line hidden
            BeginContext(3774, 61, true);
            WriteLiteral("                                            <p>Approved</p>\r\n");
            EndContext();
#line 83 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }

#line default
#line hidden
            BeginContext(3882, 93, true);
            WriteLiteral("                                        </td>\r\n                                        <td>\r\n");
            EndContext();
#line 86 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                             if (item.isUnitHeadApproved == true && item.isHrApproved == true)
                                            {

#line default
#line hidden
            BeginContext(4134, 65, true);
            WriteLiteral("                                                <p>Approved</p>\r\n");
            EndContext();
#line 89 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }
                                            else //if (item.isHrDisapprove == false && item.isUnitHeadDisapprove == false)
                                            {

#line default
#line hidden
            BeginContext(4417, 68, true);
            WriteLiteral("                                                <p>Disapproved</p>\r\n");
            EndContext();
#line 93 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }

#line default
#line hidden
            BeginContext(4532, 44, true);
            WriteLiteral("                                            ");
            EndContext();
            BeginContext(4773, 99, true);
            WriteLiteral("\r\n                                        </td>\r\n\r\n                                        <td>\r\n\r\n");
            EndContext();
#line 106 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                             if (item.isHrApproved == false && !item.isUnitHeadDisapprove == true)
                                            {
                                                

#line default
#line hidden
            BeginContext(6136, 801, true);
            WriteLiteral(@"                                                <ul class=""list-unstyled"">
                                                    <li class=""nav-item dropdown no-arrow"">
                                                        <button class=""btn btn-outline-info dropdown-toggle small"" id=""userDropdown"" role=""button"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">
                                                            Action<span class=""caret""></span>
                                                        </button>
                                                        <!-- Dropdown - User Information -->
                                                        <div class=""dropdown-menu dropdown-menu-right shadow animated--grow-in"" aria-labelledby=""userDropdown"">
");
            EndContext();
            BeginContext(7352, 60, true);
            WriteLiteral("                                                            ");
            EndContext();
            BeginContext(7412, 327, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6842a6b72984501b58d10a60b714b42", async() => {
                BeginContext(7479, 256, true);
                WriteLiteral(@"
                                                                <i class=""fas fa-cogs fa-sm fa-fw mr-2 text-gray-400""></i>
                                                                Edit
                                                            ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 131 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                                                                                         WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(7739, 182, true);
            WriteLiteral("\r\n\r\n                                                        </div>\r\n                                                    </li>\r\n                                                </ul>\r\n");
            EndContext();
#line 139 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                            }

#line default
#line hidden
            BeginContext(7968, 92, true);
            WriteLiteral("\r\n                                        </td>\r\n                                    </tr>\r\n");
            EndContext();
#line 143 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Leaves\Leaves.cshtml"
                                }

#line default
#line hidden
            BeginContext(8095, 204, true);
            WriteLiteral("                            </tbody>\r\n                        </table><br />\r\n\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<br />\r\n<br />");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<ApplicationUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Novo.Models.Entity.Leaves>> Html { get; private set; }
    }
}
#pragma warning restore 1591
