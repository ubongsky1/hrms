#pragma checksum "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "18fa046d46fc40c9c25c400919f415fc8a9bbad6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Tasks_TAskUHead), @"mvc.1.0.view", @"/Views/Tasks/TAskUHead.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Tasks/TAskUHead.cshtml", typeof(AspNetCore.Views_Tasks_TAskUHead))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp;

#line default
#line hidden
#line 2 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp.Moddel;

#line default
#line hidden
#line 3 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\_ViewImports.cshtml"
using Novo.Models.Entity;

#line default
#line hidden
#line 3 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
using Novo.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"18fa046d46fc40c9c25c400919f415fc8a9bbad6", @"/Views/Tasks/TAskUHead.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c88feaf465e480a3f60b1fca67186b9ca2f0114", @"/Views/_ViewImports.cshtml")]
    public class Views_Tasks_TAskUHead : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Novo.Models.Entity.MyTask>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("dropdown-item"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(105, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(211, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
  
    ViewData["Title"] = "List";
    var user = await UserManager.GetUserAsync(User);

#line default
#line hidden
            BeginContext(307, 289, true);
            WriteLiteral(@"


<div class=""row m-auto"">
    <div class=""col-md-12 m-auto grid-margin"">
        <div class=""card"">
            <div class=""card-body"">
                <nav aria-label=""breadcrumb"">
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item"">");
            EndContext();
            BeginContext(596, 52, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cdc78cd81c5649929c0c1bf4f3c72c90", async() => {
                BeginContext(640, 4, true);
                WriteLiteral("Home");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(648, 253, true);
            WriteLiteral("</li>\r\n                        <li class=\"breadcrumb-item active\" aria-current=\"page\">Task List</li>\r\n                    </ol>\r\n                </nav>\r\n                <div class=\"row\">\r\n                    <div class=\"col-md-12 table-responsive-md\">\r\n");
            EndContext();
            BeginContext(973, 853, true);
            WriteLiteral(@"                        <table class=""table table-striped"">
                            <thead>
                                <tr>
                                    <th>TaskName</th>
                                    <th style=""width: 500px;"">Description</th>
                                    <th style=""width: 200px;"">Assigned By</th>
                                    <th style=""width: 100px;"">StartDate</th>
                                    <th style=""width: 100px;"">EndDate</th>
                                    <th>Unit Head</th>
                                    <th>Hr.</th>
                                    <th>Status</th>
                                    <th class=""text-warning"">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
");
            EndContext();
#line 44 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                 foreach (var item in Model)
                                {

#line default
#line hidden
            BeginContext(1923, 78, true);
            WriteLiteral("                                <tr>\r\n                                    <td>");
            EndContext();
            BeginContext(2002, 13, false);
#line 47 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                   Write(item.TaskName);

#line default
#line hidden
            EndContext();
            BeginContext(2015, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2063, 20, false);
#line 48 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                   Write(item.TaskDescription);

#line default
#line hidden
            EndContext();
            BeginContext(2083, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2131, 13, false);
#line 49 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                   Write(item.AssignBy);

#line default
#line hidden
            EndContext();
            BeginContext(2144, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2192, 22, false);
#line 50 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                   Write(item.ExpectedStartDate);

#line default
#line hidden
            EndContext();
            BeginContext(2214, 47, true);
            WriteLiteral("</td>\r\n                                    <td>");
            EndContext();
            BeginContext(2262, 20, false);
#line 51 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                   Write(item.ExpectedEndDate);

#line default
#line hidden
            EndContext();
            BeginContext(2282, 53, true);
            WriteLiteral("</td>\r\n\r\n\r\n                                    <td>\r\n");
            EndContext();
#line 55 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                         if (item.isUnitHeadDisapprove == true)
                                        {

#line default
#line hidden
            BeginContext(2459, 67, true);
            WriteLiteral("                                            <p>Unsatisfactory</p>\r\n");
            EndContext();
#line 58 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }
                                        else if (item.isUnitHeadApproved == true && item.isUnitHeadDisapprove == false)
                                        {

#line default
#line hidden
            BeginContext(2733, 65, true);
            WriteLiteral("                                            <p>satisfactory</p>\r\n");
            EndContext();
#line 62 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }
                                        else
                                        {

#line default
#line hidden
            BeginContext(2930, 70, true);
            WriteLiteral("                                            <p>Awaiting Response</p>\r\n");
            EndContext();
#line 66 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }

#line default
#line hidden
            BeginContext(3043, 85, true);
            WriteLiteral("                                    </td>\r\n                                    <td>\r\n");
            EndContext();
#line 69 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                         if (item.isHrDisapprove == true)
                                        {

#line default
#line hidden
            BeginContext(3246, 67, true);
            WriteLiteral("                                            <p>Unsatisfactory</p>\r\n");
            EndContext();
#line 72 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }
                                        else if (item.isHrApproved == true && item.isHrDisapprove == false)
                                        {

#line default
#line hidden
            BeginContext(3508, 65, true);
            WriteLiteral("                                            <p>satisfactory</p>\r\n");
            EndContext();
#line 76 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }
                                        else
                                        {

#line default
#line hidden
            BeginContext(3705, 70, true);
            WriteLiteral("                                            <p>Awaiting Response</p>\r\n");
            EndContext();
#line 80 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }

#line default
#line hidden
            BeginContext(3818, 85, true);
            WriteLiteral("                                    </td>\r\n                                    <td>\r\n");
            EndContext();
#line 83 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                         if (item.isHrApproved == false && item.isUnitHeadApproved == false)
                                        {

#line default
#line hidden
            BeginContext(4056, 70, true);
            WriteLiteral("                                            <p>Undergoing Review</p>\r\n");
            EndContext();
#line 86 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }
                                        else if (item.isHrApproved == true && item.isUnitHeadApproved == true && item.isHrDisapprove == false && item.isUnitHeadDisapprove == false)
                                        {

#line default
#line hidden
            BeginContext(4394, 65, true);
            WriteLiteral("                                            <p>Satisfactory</p>\r\n");
            EndContext();
#line 90 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }

#line default
#line hidden
            BeginContext(4502, 89, true);
            WriteLiteral("                                    </td>\r\n\r\n                                    <td>\r\n\r\n");
            EndContext();
#line 95 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                         if (item.isUnitHeadApproved == false)
                                        {


#line default
#line hidden
            BeginContext(4716, 773, true);
            WriteLiteral(@"                                            <ul class=""list-unstyled"">
                                                <li class=""nav-item dropdown no-arrow"">
                                                    <button class=""btn btn-outline-info dropdown-toggle small"" id=""userDropdown"" role=""button"" data-toggle=""dropdown"" aria-haspopup=""true"" aria-expanded=""false"">
                                                        Action<span class=""caret""></span>
                                                    </button>
                                                    <!-- Dropdown - User Information -->
                                                    <div class=""dropdown-menu dropdown-menu-right shadow animated--grow-in"" aria-labelledby=""userDropdown"">
");
            EndContext();
            BeginContext(5804, 56, true);
            WriteLiteral("                                                        ");
            EndContext();
            BeginContext(5860, 315, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "3aca5a9777974a9aba63cb52c0feb843", async() => {
                BeginContext(5927, 244, true);
                WriteLiteral("\r\n                                                            <i class=\"fas fa-cogs fa-sm fa-fw mr-2 text-gray-400\"></i>\r\n                                                            Edit\r\n                                                        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 109 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                                                                                     WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6175, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(6476, 168, true);
            WriteLiteral("\r\n                                                    </div>\r\n                                                </li>\r\n                                            </ul>\r\n");
            EndContext();
#line 121 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                        }

#line default
#line hidden
            BeginContext(6687, 84, true);
            WriteLiteral("\r\n                                    </td>\r\n                                </tr>\r\n");
            EndContext();
#line 125 "C:\Users\Ubong Umoh\Downloads\hrms\NovoApp\Views\Tasks\TAskUHead.cshtml"
                                }

#line default
#line hidden
            BeginContext(6806, 208, true);
            WriteLiteral("                            </tbody>\r\n                        </table><br />\r\n\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<br />\r\n<br />\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public UserManager<ApplicationUser> UserManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public SignInManager<ApplicationUser> SignInManager { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Novo.Models.Entity.MyTask>> Html { get; private set; }
    }
}
#pragma warning restore 1591
