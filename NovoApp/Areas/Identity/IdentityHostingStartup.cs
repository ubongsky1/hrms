﻿using System;
using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(NovoApp.Areas.Identity.IdentityHostingStartup))]
namespace NovoApp.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}