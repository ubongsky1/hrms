﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Text;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace NovoApp.Controllers
{
    public class TasksController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public TasksController(ApplicationDbContext context,
                                SignInManager<ApplicationUser> signInManager,
                                UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }




        // GET: Tasks
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Tasks.Where(x => x.AssignToId == user.Id).ToListAsync());
            //return View(await _context.Tasks.OrderByDescending(x=>x.Id).ToListAsync());
        }

        [Authorize(Roles = "HR, HRHead, Admin, SuperAdmin")]
        public async Task<IActionResult> TAskHR()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Tasks.OrderByDescending(x=>x.Id).ToListAsync());
        }

        [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin")]
        public async Task<IActionResult> TAskUHead()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Tasks.OrderByDescending(x => x.Id).ToListAsync());
        }

        // GET: Tasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myTask = await _context.Tasks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (myTask == null)
            {
                return NotFound();
            }

            return View(myTask);
        }

        // GET: Tasks/Create
        public IActionResult Create()
        {

            ViewBag.Employee = new SelectList(_context.EmploymentDetails.Where(x => x.EmployeeId != 0), "EmployeeId", "Fullname");
            ViewData["AssignToId"] = new SelectList(_context.EmploymentDetails.Where(x => x.EmployeeId != 0), "EmployeeId", "Fullname");
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MyTask myTask)
        {

           
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                _context.Add(myTask);
                await _context.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                var fromEmail = user.Email;
                var fromName = user.FirstName + ' ' + user.LastName;
                var eee = (from a in _context.Users
                           join b in _context.Tasks on a.Id equals b.AssignToId
                           where a.Id == b.AssignToId
                           select new
                           {
                               a.Email,
                               a.FirstName,
                               a.LastName

                           }).FirstOrDefault();

                var check = _context.Users.Where(x => x.Id == myTask.AssignToId).Select(s => new { s.Email }).FirstOrDefault();

                var sample = check.Email;

                var toEmail = eee.Email.ToString();
                var toName = eee.FirstName + eee.LastName;
                //instantiate a new MimeMessage
                var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(sample));


                var tt = "akinbamidelea@novohealthafrica.org";
                message.Bcc.Add(new MailboxAddress(tt));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                //E-mail subject 
                message.Subject = "New Task Request ";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +

               "<p> This is to notify that a new Task request has been assigned to you by " + user.FirstName + " " + user.LastName +

               "<br/>on </a>" + myTask.CreateDate + ".</p>" +
               "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Tasks/Details" + myTask.Id + "'>Here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.office365.com", 587, false);
                    emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }

                if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("TAskUHead", "Tasks");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("TAskHR", "Tasks");
                }
                return RedirectToAction(nameof(Index));
            }
            ViewBag.Employee = new SelectList(_context.EmploymentDetails.Where(x => x.EmployeeId != 0), "EmployeeId", "Fullname", myTask.AssignToId);
            ViewData["AssignToId"] = new SelectList(_context.EmploymentDetails.Where(x => x.EmployeeId != 0), "EmployeeId", "Fullname", myTask.AssignToId);
            return View(myTask);
        }

        // GET: Tasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myTask = await _context.Tasks.FindAsync(id);
            if (myTask == null)
            {
                return NotFound();
            }
            return View(myTask);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit( MyTask myTask)
        {
         

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(myTask);
                    await _context.SaveChangesAsync();

                    if (myTask.isUnitHeadApproved==true && myTask.isHrApproved==true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;
                        var eee = (from a in _context.Users
                                   join b in _context.Tasks on a.Id equals b.AssignToId
                                   where a.Id == b.AssignToId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName + eee.LastName;
                        var check = _context.Users.Where(x => x.Id == myTask.AssignToId).Select(s => new { s.Email }).FirstOrDefault();

                        var sample = check.Email;
                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Task Request ";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that your Task request has been approved satisfactory by your Unit Head and the HR. " +

                       "<br/>on </a>" + myTask.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Tasks/Details" + myTask.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (myTask.isUnitHeadApproved==true && myTask.isUnitHeadDisapprove==false)
                    {

                        var user = await _userManager.GetUserAsync(User);
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;
                        var eee = (from a in _context.Users
                                   join b in _context.Tasks on a.Id equals b.AssignToId
                                   where a.Id == b.AssignToId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName + eee.LastName;
                        var check = _context.Users.Where(x => x.Id == myTask.AssignToId).Select(s => new { s.Email }).FirstOrDefault();

                        var sample = check.Email;

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Task Request ";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that your Task request has been approved satisfactory by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + myTask.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Tasks/Details" + myTask.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (myTask.isUnitHeadDisapprove==true && myTask.isUnitHeadApproved==false)
                    {

                        var user = await _userManager.GetUserAsync(User);
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;
                        var eee = (from a in _context.Users
                                   join b in _context.Tasks on a.Id equals b.AssignToId
                                   where a.Id == b.AssignToId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName + eee.LastName;
                        var check = _context.Users.Where(x => x.Id == myTask.AssignToId).Select(s => new { s.Email }).FirstOrDefault();

                        var sample = check.Email;
                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Task Request ";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that your Task request has been approved unsatisfactory by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + myTask.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Tasks/Details" + myTask.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (myTask.isHrApproved==true && myTask.isHrDisapprove==false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;
                        var eee = (from a in _context.Users
                                   join b in _context.Tasks on a.Id equals b.AssignToId
                                   where a.Id == b.AssignToId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName + eee.LastName;
                        var check = _context.Users.Where(x => x.Id == myTask.AssignToId).Select(s => new { s.Email }).FirstOrDefault();

                        var sample = check.Email;
                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Task Request ";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify that your Task request has been approved satisfactory by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + myTask.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Tasks/Details" + myTask.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MyTaskExists(myTask.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("TAskUHead", "Tasks");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("TAskHR", "Tasks");
                }
                return RedirectToAction(nameof(Index));
            }
            return View(myTask);
        }

        // GET: Tasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var myTask = await _context.Tasks
                .FirstOrDefaultAsync(m => m.Id == id);
            if (myTask == null)
            {
                return NotFound();
            }

            return View(myTask);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var myTask = await _context.Tasks.FindAsync(id);
            _context.Tasks.Remove(myTask);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MyTaskExists(int id)
        {
            return _context.Tasks.Any(e => e.Id == id);
        }
    }
}
