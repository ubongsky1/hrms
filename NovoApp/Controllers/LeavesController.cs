﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Text;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using MailKit.Net.Smtp;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace NovoApp.Controllers
{
    public class LeavesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public LeavesController(ApplicationDbContext context, 
                                SignInManager<ApplicationUser> signInManager, 
                                UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: Leaves
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var leave = _context.LeaveTypes.GetType();
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Leaves.Where(x => x.EmployeeEmail == user.Email).ToListAsync());
        }

        [Authorize(Roles = "HR, HRHead, Admin, SuperAdmin")]
        public async Task<IActionResult> Leaves()
        {

            var user = await _userManager.GetUserAsync(User);

            ViewBag.AwaitingApproval = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Disapproved = _context.Leaves.Where(x => x.isUnitHeadDisapprove == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Approved = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == true).Count();
            var Leaves = _context.Leaves.Where(x => x.isUnitHeadApproved == true || x.isUnitHeadDisapprove == true && x.IsDelete == false).ToList();

            return View(Leaves);
        }

        [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin")]
        public async Task<IActionResult> LeavesList()
        {

            var user = await _userManager.GetUserAsync(User);
            

            ViewBag.AwaitingHrApproval = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Approved = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == true).Count();
            
            var LeavesList = _context.Leaves.Where(x => x.IsDelete == false && x.UnitsId == user.UnitsId).ToList();
            
            
            return View(LeavesList);
        }

        // GET: Leaves/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context.Leaves
                .Include(l => l.LeaveType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // GET: Leaves/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);

            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type");
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name");

            ViewBag.CurrentLeaves = Int32.Parse(_context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() || x.FirstName.ToLower() == user.LastName.ToLower()).Select(x => x.CurrentLeaveDays).FirstOrDefault());
            ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == user.Email).Count();
            if(_context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() || x.FirstName.ToLower() == user.LastName.ToLower()).Select(x => x.NumberOfOutstandingLeaves).FirstOrDefault() == String.Empty)
            {
                ViewBag.OutstandingLeaves = 0;
            }
            else
            {
                ViewBag.OutstandingLeaves = Int32.Parse(_context.LeaveUploads.Where(x => x.FirstName == user.FirstName || x.FirstName == user.LastName).Select(x => x.NumberOfOutstandingLeaves).FirstOrDefault());
            }

            ViewBag.ToEmail = user.FirstName + ' ' + user.LastName;

          
            var eee = (from a in _context.Users
                       join b in _context.UserRoles on a.Id equals b.UserId
                       join c in _context.Roles on b.RoleId equals c.Id
                       where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                       select new
                       {
                           a.Email,
                           a.FirstName,
                           a.LastName

                       }).FirstOrDefault();

            ViewBag.FromEmail = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

            return View();
        }

        // POST: Leaves/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StartDate,EndDate,EmployeeEmail,EmployeeId,LeaveTypeId,Purpose,isUnitHeadApproved,isHrApproved,TotalLeave,LeaveUtilized,LeaveRemaining,LeaveRequestStatus,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete,HandOver,UnitsId")] Leaves leaves)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                _context.Add(leaves);
                await _context.SaveChangesAsync();
                var user = await _userManager.GetUserAsync(User);
                ViewBag.ToEmail = user.Email;
                var fromEmail = user.Email;
                var fromName = user.FirstName + ' ' + user.LastName;

                var eee = (from a in _context.Users
                           join b in _context.UserRoles on a.Id equals b.UserId
                           join c in _context.Roles on b.RoleId equals c.Id
                           where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                           select new
                           {
                               a.Email,
                               a.FirstName,
                               a.LastName

                           }).FirstOrDefault();

                ViewBag.FromEmail = eee.Email.ToString();
                var toEmail = eee.Email.ToString();
                var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                
                //instantiate a new MimeMessage
                var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(toName, toEmail));

                   
                    var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "New Service Request Added";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify you that a new leave request has been sent by " + user.FirstName + " " + user.LastName +

                   "<br/>on </a>" + leaves.CreateDate + ".</p>" +
                   "Unit Head can see more information on the leave request by Clicking <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                    };

                    //Configure the e-mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        emailClient.Connect("smtp.office365.com", 587, false);
                        emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                

                return RedirectToAction(nameof(Index));
            }
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return RedirectToAction("Create", "LeaveHandovers");
        }



        // GET: Leaves/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var leaves = await _context.Leaves.FindAsync(id);
            if (leaves == null)
            {
                return NotFound();
            }
            
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return View(leaves);
        }

        // POST: Leaves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StartDate,EndDate,EmployeeEmail,LeaveTypeId,Purpose,isUnitHeadApproved,isHrApproved,TotalLeave,LeaveUtilized,LeaveRemaining,LeaveRequestStatus,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete,HandOver,UnitsId,EmployeeId,isUnitHeadDisapprove,isHrDisapprove")] Leaves leaves)
        {
            if (id != leaves.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leaves);
                    await _context.SaveChangesAsync();
                    if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        //var use = Int32.Parse(_context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId).LastOrDefault().ToString());


                        //var uuuu = _context.Users.Find(use);

                        //var ttyt = uuuu.Email.ToString();
                        //var tttt = uuuu.FirstName.ToString() + ' ' + uuuu.LastName.ToString(); 
                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();


                        //var tttt = _context.Users.Find(use).Email.ToString();
                        
                        
                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //var ffff = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId);

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {

                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head and the HR." +
                       "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but rejected by the Head of Human Resource Unit." +
                      "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "HR"
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();
                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();


                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(toName, toEmail));

                        //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Leave Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but awaiting approval by the Head of Human Resource Unit." +
                       "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +





                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName
                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been rejected by your Unit Head." +
                      "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {
                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LeavesExists(leaves.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }


                if(_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("leaveslist","leaves");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("leaves", "leaves");
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return View(leaves);
        }

        // GET: Leaves/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context.Leaves
                .Include(l => l.LeaveType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // POST: Leaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leaves = await _context.Leaves.FindAsync(id);
            _context.Leaves.Remove(leaves);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LeavesExists(int id)
        {
            return _context.Leaves.Any(e => e.Id == id);
        }
              
    }
}
