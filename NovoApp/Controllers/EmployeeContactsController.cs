﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class EmployeeContactsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmployeeContactsController(ApplicationDbContext context, SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: EmployeeContacts1
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.EmployeeContacts.Include(e => e.Lga).Include(e => e.Regions).Include(e => e.State);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: EmployeeContacts1/Details/5
        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);
           
            var employeeContact = await _context.EmployeeContacts
                .Where(x => x.EmployeeId == user.Id)
                .Include(e => e.Lga)
                .Include(e => e.Regions)
                .Include(e => e.State)
                .FirstOrDefaultAsync();
            if (employeeContact == null)
            {
                return RedirectToAction("Create", "EmployeeContacts");
                
            }

            return View(employeeContact);
        }


        // GET: EmployeeContacts1/Details/5
        public async Task<IActionResult> Detailss(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var employeeContact = await _context.EmployeeContacts
                .Where(x => x.Id == id)
                .Include(e => e.Lga)
                .Include(e => e.Regions)
                .Include(e => e.State)
                .FirstOrDefaultAsync();
            if (employeeContact == null)
            {
                return RedirectToAction("Create", "EmployeeContacts");
                
            }

            return View(employeeContact);
        }
        // GET: EmployeeContacts1/Create
        public IActionResult Create()
        {
            ViewData["LgaId"] = new SelectList(_context.Lga, "Id", "Name");
            ViewData["RegionId"] = new SelectList(_context.Regions, "Id", "Name");
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Name");
            return View();
        }

        // POST: EmployeeContacts1/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,Address,City,StateId,StateOfOrigin,LgaId,RegionId,CountryOfOrigin,BeneficiaryName,BeneficiaryNo,BeneficiaryAddress,EmergencyName,EmergencyNo,EmergencyAddress,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] EmployeeContact employeeContact)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                _context.Add(employeeContact);
                await _context.SaveChangesAsync();
               
                return RedirectToAction("Create", "Qualifications");
            }
            ViewData["LgaId"] = new SelectList(_context.Lga, "Id", "Name", employeeContact.LgaId);
            ViewData["RegionId"] = new SelectList(_context.Regions, "Id", "Name", employeeContact.RegionId);
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Name", employeeContact.StateId);
            //return View(employeeContact);
            return RedirectToAction("Create", "Qualifications");
        }

        // GET: EmployeeContacts1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeContact = await _context.EmployeeContacts.FindAsync(id);
            if (employeeContact == null)
            {
                return NotFound();
            }
            ViewData["LgaId"] = new SelectList(_context.Lga, "Id", "Name", employeeContact.LgaId);
            ViewData["RegionId"] = new SelectList(_context.Regions, "Id", "Name", employeeContact.RegionId);
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Name", employeeContact.StateId);
            return View(employeeContact);
        }

        // POST: EmployeeContacts1/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,Address,City,StateId,StateOfOrigin,LgaId,RegionId,CountryOfOrigin,BeneficiaryName,BeneficiaryNo,BeneficiaryAddress,EmergencyName,EmergencyNo,EmergencyAddress,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] EmployeeContact employeeContact)
        {
            if (id != employeeContact.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid || !ModelState.IsValid)
            {
                try
                {
                    _context.Update(employeeContact);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeContactExists(employeeContact.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Create", "EmployeeContacts");
            }
            ViewData["LgaId"] = new SelectList(_context.Lga, "Id", "Name", employeeContact.LgaId);
            ViewData["RegionId"] = new SelectList(_context.Regions, "Id", "Name", employeeContact.RegionId);
            ViewData["StateId"] = new SelectList(_context.States, "Id", "Name", employeeContact.StateId);
            return View(employeeContact);
        }

        // GET: EmployeeContacts1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employeeContact = await _context.EmployeeContacts
                .Include(e => e.Lga)
                .Include(e => e.Regions)
                .Include(e => e.State)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeeContact == null)
            {
                return NotFound();
            }

            return View(employeeContact);
        }

        // POST: EmployeeContacts1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employeeContact = await _context.EmployeeContacts.FindAsync(id);
            _context.EmployeeContacts.Remove(employeeContact);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeContactExists(int id)
        {
            return _context.EmployeeContacts.Any(e => e.Id == id);
        }
    }
}
