﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Novo.Models.Entity;
//using System.Diagnostics;
using System.IO;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using NovoClients.DataAccess.Data;
using Microsoft.AspNetCore.Identity;

namespace NovoClients.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public HomeController(ApplicationDbContext context, IHostingEnvironment hostingEnvironment,
                                                SignInManager<ApplicationUser> signInManager,
                                                UserManager<ApplicationUser> userManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;

        }
        ////[Route("Index")]
        //public IActionResult Index()
        //{
        //    return View();
        //}

        //[Route("Dashboard")]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
           
           
            int AppUser = _context.EmploymentDetails.Where(x => x.Email.Contains(user.Email)).Count();
            if (AppUser > 0)
            {

                return View(user);
            }

            return RedirectToAction("Create", "EmploymentDetails1");
        }

        //[Route("Verify")]
        public IActionResult Verify()
        {
            return View();
        }

        // [Route("AuthorizationCodes")]
        public IActionResult AuthorizationCodes()
        {
            return View();
        }

        // [Route("CaptureBills")]
        public IActionResult CaptureBills()
        {
            return View();
        }

        //[Route("SavedBills")]
        public IActionResult SavedBills()
        {
            return View();
        }

        // [Route("SubmittedBills")]
        public IActionResult SubmittedBills()
        {
            return View();
        }

        // [Route("Appointments")]
        public IActionResult Appointments()
        {
            return View();
        }

        // [Route("Notifications")]
        public IActionResult Notifications()
        {
            return View();
        }

        //  [Route("Profile")]
        public IActionResult Profile()
        {
            return View();
        }

        //[Route("Support")]
        public IActionResult Support()
        {
            return View();
        }

 
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public ActionResult OnPostImport()
        {
            IFormFile file = Request.Form.Files[0];
            string folderName = "Upload";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(1); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }
                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;
                    sb.Append("<table class='table'><tr>");
                    for (int j = 0; j < cellCount; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                        sb.Append("<th>" + cell.ToString() + "</th>");
                    }
                    sb.Append("</tr>");
                    sb.AppendLine("<tr>");
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                                sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
                        }
                        sb.AppendLine("</tr>");
                    }
                    sb.Append("</table>");
                }
            }
            return this.Content(sb.ToString());
        }

        public async Task<IActionResult> InsertFromExcel(IFormFile theExcel)
        {
            ExcelPackage excel;

            using (var memorystream = new MemoryStream())
            {
                await theExcel.CopyToAsync(memorystream);
                excel = new ExcelPackage(memorystream);
            }

            var worksheet = excel.Workbook.Worksheets[1];
            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;
            for (int row = start.Row; row <= end.Row; row++)
            {
                LeaveUpload a = new LeaveUpload
                {
                    //Id= int.Parse(worksheet.Cells[row, 1].Text),
                    Surname = worksheet.Cells[row, 1].Text,
                    FirstName = worksheet.Cells[row, 2].Text,
                    StaffNumber = worksheet.Cells[row, 3].Text,
                    Location = worksheet.Cells[row, 4].Text,
                    NumberOfOutstandingLeaves = worksheet.Cells[row, 5].Text,
                    CurrentLeaveDays = worksheet.Cells[row, 6].Text,

                };

                _context.LeaveUploads.Add(a);
            };
            _context.SaveChanges();
            return RedirectToAction(nameof(About));
        }
    }
}
