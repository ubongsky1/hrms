﻿using Microsoft.AspNetCore.Identity;
using Novo.Models.Entity;
using System;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace NovoClients.DataAccess.Data
{
    public class DummyData
    {
        public static async Task Initialize(ApplicationDbContext context,
                              UserManager<ApplicationUser> userManager,
                              RoleManager<ApplicationRole> roleManager)
        {
            context.Database.EnsureCreated();


            var Code = Guid.NewGuid().ToString("n").Substring(0, 4);
            int adminId1 = 0;
            int adminId2 = 0;
            int adminId3 = 0;
            int adminId4 = 0;
            int adminId5 = 0;
            int adminId6 = 0;
            int adminId7 = 0;
            int adminId8 = 0;



            string role1 = "Admin";
            string desc1 = "This is the administrators role";

            string role2 = "User";
            string desc2 = "This is the users role";

            string role3 = "HR";
            string desc3 = "This is the HR role";

            string role4 = "HR Head";
            string desc4 = "This is the HRHead role";

            string role5 = "Unit Head";
            string desc5 = "This is the UnitHead role";

            string role6 = "Dept Head";
            string desc6 = "This is the Dept Head role";

            string role7 = "Regional Head";
            string desc7 = "This is the Regional Head role";

            string role8 = "SuperAdmin";
            string desc8 = "This is the Super Admin role";



            string password = "P@$$w0rd";

            if (await roleManager.FindByNameAsync(role1) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role1, desc1, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role3) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role3, desc3, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role4) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role4, desc4, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role5) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role5, desc5, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role6) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role6, desc6, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role7) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role7, desc7, DateTime.Now));
            }
            if (await roleManager.FindByNameAsync(role8) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role8, desc8, DateTime.Now));
            }
            if (await userManager.FindByNameAsync("akinbamidelet@outlook.com") == null)
            {
                var user = new ApplicationUser
                {
                  
                    UserName = "akinbamidelet@outlook.com",
                    Email = "akinbamidelet@outlook.com",
                    PhoneNumber = "09080086100",
                    FirstName = "TEST",
                    LastName = "Test",
                    PasswordHash = "P@$$w0rd",
                    UnitsId = 1


                };


                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                adminId1 = user.Id;
            }



            if (await roleManager.FindByNameAsync(role2) == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role2, desc2, DateTime.Now));
            }
            if (await userManager.FindByNameAsync("akinbamidelet@gmail.com") == null)
            {
                var user = new ApplicationUser
                {

                    UserName = "akinbamidelet@gmail.com",
                    Email = "akinbamidelet@gmail.com",
                    PhoneNumber = "090123456789",
                    FirstName = "Admin",
                    LastName = "Test",
                    PasswordHash = "P@$$w0rd",
                    UnitsId = 1


                };

                var result = await userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
                adminId2 = user.Id;
            }




        }
    }
}
