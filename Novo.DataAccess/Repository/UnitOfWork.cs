﻿using Novo.DataAccess.Repository.IRepository;
using NovoClients.DataAccess.Data;

namespace NovoClients.DataAccess.Repository
{
     public class UnitOfWork : IUnitOfWork
     {
        private readonly ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
           
        }

        //public IClaimRepository Claim { get; private set; }
     
        //public IClaimBatchRepository ClaimBatch { get; private set; }

        //public IAuthorizationCodesRepository AuthorizationCodes { get; private set; }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
