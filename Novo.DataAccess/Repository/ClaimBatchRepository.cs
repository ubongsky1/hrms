﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NovoClients.DataAccess.Data;
using Novo.Models.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Novo.DataAccess.Repository;
using Novo.DataAccess.Repository.IRepository;

namespace NovoClients.DataAccess.Repository
{
    //public class ClaimBatchRepository : Repository<ClaimBatch>, IClaimBatchRepository
    //{
    //    private readonly ApplicationDbContext _db;

    //    public ClaimBatchRepository(ApplicationDbContext db) : base(db)
    //    {
    //        _db = db;
    //    }

    //    public new object Get(int ProviderId)
    //    {
    //        var claimbatch = (from h in _db.ClaimBatch
    //                          join i in _db.Claim on h.Id equals i.ClaimBatchId
    //                          join k in _db.Provider on i.ProviderId equals k.Id
    //                          join l in _db.State on k.StateId equals l.Id
    //                          join m in _db.Lga on k.LgaId equals m.Id
    //                          join n in _db.Zone on l.Zone equals n.Id
    //                          join o in _db.Month on h.Month equals o.Id

    //                          where h.ProviderId == ProviderId && h.Status == "Vetting" && h.IsDeleted == false
    //                          select new
    //                          {

    //                              drugAmount = i.ClaimDrug.Sum(x => x.InitialAmount),
    //                              serviceAmount = i.ClaimService.Sum(x => x.InitialAmount),
    //                              initialamount = i.ClaimDrug.Sum(x => x.InitialAmount) + i.ClaimService.Sum(x => x.InitialAmount),
    //                              processedamount = i.ClaimDrug.Sum(x => x.VettedAmount) + i.ClaimService.Sum(x => x.VettedAmount),
    //                              difference = (i.ClaimDrug.Sum(x => x.InitialAmount) + i.ClaimService.Sum(x => x.InitialAmount)) -
    //                              (i.ClaimDrug.Sum(x => x.VettedAmount) + i.ClaimService.Sum(x => x.VettedAmount)),
    //                              claimperiod = o.Name + "," + h.Year,
    //                              batch = h.Batch,
    //                              claimcount = h.Claim,
    //                              ProviderName = k.Name,
    //                              ProviderLga = m.Name,
    //                              ProviderState = l.Name,
    //                              ProviderZone = n.Name,
    //                              i.Id,
    //                              i.ClaimsSerialNo,
    //                              i.EnrolleeFullname,
    //                              i.EnrolleeCompanyName,
    //                              i.EnrolleePolicyNumber,
    //                              i.EnrolleePlan,
    //                              i.Diagnosis,
    //                              i.Tag,
    //                              i.Durationoftreatment,
    //                              i.IsDeleted,
    //                              i.ProviderId,
    //                              i.Status,
    //                              i.VettedDate,
    //                              i.CreatedOn,
    //                              i.ServiceDate
    //                          }).ToList();

    //        return claimbatch;
    //    }
    //}
}
