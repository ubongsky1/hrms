﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        //IClaimRepository Claim { get; }
        //IClaimBatchRepository ClaimBatch { get; }
        //IAuthorizationCodesRepository AuthorizationCodes { get; }

        void Save();
    }
}
